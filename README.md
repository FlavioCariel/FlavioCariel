👋 Olá, eu sou o Flávio!

Bacharelando em Ciência e Tecnologia na UFABC, atualmente estou explorando o mundo da tecnologia como estagiário na COTIC-SME, localizada em São Paulo. Tenho grande interesse por Data Science e Desenvolvimento Web, sempre buscando desafios que me permitam expandir meus horizontes nesses campos fascinantes.

🌱 Atualmente

    🎓 Cursando bacharelado em Ciência e Tecnologia na UFABC.
    🖥️ Estagiário na COTIC-SME, São Paulo.

💼 Experiência

Possuo experiência nas seguintes áreas:

    Linguagens de Programação: Python e JavaScript.
    Data Science: Domínio avançado da biblioteca Pandas.
    Desenvolvimento Web: Aprendendo a utilizar o framework Django.

🚀 Habilidades

    Pensamento Abstrato: Abordo desafios com uma mentalidade abrangente, sempre buscando soluções inteligentes.
    Adaptabilidade: Capaz de aproveitar as especificidades das tecnologias para criar soluções eficientes e inovadoras.
    Desenvolvimento Ágil: Comprometido em encontrar soluções práticas e eficazes.

🤝 Contato

Estou sempre aberto a novas conexões e colaborações. Fique à vontade para entrar em contato!

    📧 E-mail: flaviocariel@gmail.com

 
